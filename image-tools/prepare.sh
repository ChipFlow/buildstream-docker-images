#!/bin/sh
# Install dependencies required to run the buildstream x86image plugin

set -eu
apk add --no-cache parted mtools e2fsprogs dosfstools syslinux nasm \
    autoconf gcc g++ make gawk bc linux-headers
